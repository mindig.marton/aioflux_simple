import asyncio
import colorsys
from functools import partial
import logging
import socket

_LOGGER = logging.getLogger(__name__)


class FluxMessage:
    TYPE_SYNC_TIME = 0x10
    TYPE_GET_TIME = 0x11
    TYPE_SET_TIME_CONTENT = 0x21
    TYPE_GET_TIME_CONTENT = 0x22
    TYPE_SET_COLOR_PERSIST = 0x31
    TYPE_SET_COLOR = 0x41
    TYPE_SET_CUSTOM_MODE = 0x51
    TYPE_GET_CUSTOM_MODE = 0x52
    TYPE_SET_PRESET = 0x61
    TYPE_SET_STATE = 0x71
    TYPE_GET_STATE = 0x81
    TYPE_GET_STATE_1 = 0x8a
    TYPE_GET_STATE_2 = 0x8b

    COMMAND_TYPE_LOCAL = 0x0f
    COMMAND_TYPE_REMOTE = 0xf0

    SET_COLOR_TYPE_ALL = 0x00
    SET_COLOR_TYPE_W = 0x0f
    SET_COLOR_TYPE_RGB = 0xf0

    KEY_PAUSE = 0x20
    KEY_PLAY = 0x21
    KEY_SWITCH = 0x22
    KEY_ON = 0x23
    KEY_OFF = 0x24

    KEY_CHANGE_GRADUAL = 0x3a
    KEY_CHANGE_JUMP = 0x3b
    KEY_CHANGE_FLASH = 0x3c

    MODE_CUSTOM = 0x60
    MODE_STATIC = 0x61
    MODE_MUSIC = 0x62
    MODE_TESTING = 0x63


class FluxLedRgbwWifiProtocol(asyncio.Protocol):
    DEFAULT_PORT = 5577

    def __init__(self, loop, device_info,
                 state_change_callback, disconnect_callback):
        self._loop = loop  # type: asyncio.AbstractEventLoop
        self._device_info = device_info  # type: FluxDiscoveryDeviceInfo
        self._transport = None  # type: asyncio.Transport

        self._state_change_cb = state_change_callback  # type: callable
        self._disconnect_cb = disconnect_callback  # type: callable

        self._is_on = False
        self._device_id = 0
        self._red = 0
        self._green = 0
        self._blue = 0
        self._warm_white = 0

    def connection_made(self, transport):
        self._transport = transport
        _LOGGER.debug('connected')

        self.query_state()

    def data_received(self, data):
        _LOGGER.debug('received data: %s', data)

        message_type = data[0]

        if message_type is FluxMessage.TYPE_GET_STATE:
            mode = data[3]
            if mode != FluxMessage.MODE_STATIC:
                raise Exception("Device must be in STATIC mode")

            self.state_received(data)

    def write(self, data: bytearray):
        _LOGGER.debug('writing data: %s', repr(data))

        if self._transport is not None:
            checksum = sum(data) & 0xFF
            data.append(checksum)

            self._transport.write(data)

    def connection_lost(self, exc):
        if exc:
            _LOGGER.error(exc, exc_info=True)
        else:
            _LOGGER.info('disconnected because of close/abort.')
        if self._disconnect_cb:
            self._disconnect_cb(exc)

    @property
    def device_info(self):
        return self._device_info

    @property
    def is_on(self):
        return self._is_on

    @property
    def rgbw(self):
        return self._red, self._green, self._blue, self._warm_white

    @property
    def brightness(self):
        """Return current brightness 0-255.

        For warm white return current led brightness. For RGB
        calculate the HSV and return the 'value'.
        """
        _, _, v = colorsys.rgb_to_hsv(self._red, self._green, self._blue)
        return v

    @property
    def white_temperature(self):
        return 0

    @property
    def device_id(self):
        return self._device_id

    @property
    def version(self):
        return 4

    def turn_on(self):
        msg = bytearray([FluxMessage.TYPE_SET_STATE, FluxMessage.KEY_ON,
                         FluxMessage.COMMAND_TYPE_LOCAL])

        self.write(msg)

    def turn_off(self):
        msg = bytearray([FluxMessage.TYPE_SET_STATE, FluxMessage.KEY_OFF,
                         FluxMessage.COMMAND_TYPE_LOCAL])

        self.write(msg)

    def query_state(self):
        msg = bytearray([FluxMessage.TYPE_GET_STATE,
                         FluxMessage.TYPE_GET_STATE_1,
                         FluxMessage.TYPE_GET_STATE_2])

        self.write(msg)

    def state_received(self, data):
        self._device_id = int(data[1])
        self._is_on = data[2] == FluxMessage.KEY_ON
        # rx[3] mode
        # rx[4] state
        # rx[5] speed
        self._red = int(data[6])
        self._green = int(data[7])
        self._blue = int(data[8])
        self._warm_white = int(data[9])
        # rx[10] version
        # rx[11] cool white
        # rx[12] status
        # rx[13] checksum

        self._state_change_cb(self)

    def set_white_temperature(self, temperature, brightness):
        temperature = max(temperature - 2700, 0)
        warm = 255 * (1 - (temperature / 3800))
        cool = min(255 * temperature / 3800, 255)

        warm *= brightness / 255
        cool *= brightness / 255

        self.set_rgbw(warm_white=warm, cool_white=cool)

    def set_rgbw(self, red=None, green=None, blue=None,
                 warm_white=None, cool_white=None,
                 brightness=None):
        msg = bytearray([FluxMessage.TYPE_SET_COLOR_PERSIST])

        if cool_white is not None:
            if brightness is not None:
                cool_white *= brightness / 255

            msg.append(int(cool_white))  # r
            msg.append(int(cool_white))  # g
            msg.append(int(cool_white))  # b

            self._red = cool_white
            self._green = cool_white
            self._blue = cool_white
        elif red is not None and green is not None and blue is not None:
            if brightness is not None:
                hsv = colorsys.rgb_to_hsv(red, green, blue)
                red, green, blue = colorsys.hsv_to_rgb(hsv[0], hsv[1],
                                                       brightness)
            msg.append(int(red))
            msg.append(int(green))
            msg.append(int(blue))

            self._red = red
            self._green = green
            self._blue = blue
        else:
            msg.append(0)  # r
            msg.append(0)  # g
            msg.append(0)  # b

        if warm_white is not None:
            if brightness is not None:
                warm_white *= brightness / 255

            msg.append(int(warm_white))

            self._warm_white = warm_white
        else:
            msg.append(0)
            self._warm_white = 0

        msg.append(FluxMessage.SET_COLOR_TYPE_ALL)
        msg.append(FluxMessage.COMMAND_TYPE_LOCAL)

        self.write(msg)

    def __str__(self):
        return "FluxLedRgbwWifiProtocol[device_info: {0} " \
               "rgbw: {1} version: {2}]".format(self.device_info,
                                                self.rgbw,
                                                self.version)

    @staticmethod
    def create_connection(loop, device_info,
                          state_change_callback=None,
                          disconnect_callback=None):
        _LOGGER.debug('Initializing connection to %s', device_info)

        protocol = partial(
            FluxLedRgbwWifiProtocol,
            loop,
            device_info,
            disconnect_callback=disconnect_callback,
            state_change_callback=state_change_callback
        )

        return loop.create_connection(
            protocol,
            device_info.device_ip, FluxLedRgbwWifiProtocol.DEFAULT_PORT)


class FluxDiscoveryDeviceInfo:
    def __init__(self, device_ip, device_id, model):
        self.device_ip = device_ip  # type: str
        self.device_id = device_id  # type: str
        self.model = model  # type: str

    def __str__(self):
        return "FluxDiscoveryDeviceInfo[ip: {0} id: {1} model: {2}]".format(
            self.device_ip, self.device_id, self.model)


class FluxDiscoveryServerProtocol(asyncio.DatagramProtocol):
    DISCOVERY_MSG = "HF-A11ASSISTHREAD"
    DISCOVERY_PORT = 48899

    def __init__(self, loop, broadcast_ip, discovery_interval,
                 device_discovered):
        self._transport = None  # type: asyncio.DatagramTransport
        self._loop = loop  # type: asyncio.AbstractEventLoop

        self._discovery_interval = discovery_interval
        self._broadcast_ip = broadcast_ip  # type: str

        if device_discovered is None:
            raise Exception("device_discovered callback must be specified!")

        self._device_discovered_cb = device_discovered  # type: callable

    def connection_made(self, transport):
        self._transport = transport

        sock = transport.get_extra_info("socket")
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

        self.broadcast()

    def datagram_received(self, data, addr):
        data = data.decode('ascii')
        if data == FluxDiscoveryServerProtocol.DISCOVERY_MSG:
            return

        device_info = data.split(',')

        if len(device_info) == 3:
            self._device_discovered_cb(FluxDiscoveryDeviceInfo(*device_info))
        else:
            _LOGGER.warning("Received device info data length is incorrect")

    def broadcast(self):
        self._transport.sendto(self.DISCOVERY_MSG.encode('ascii'),
                               (self._broadcast_ip, self.DISCOVERY_PORT))

        self._loop.call_later(self._discovery_interval, self.broadcast)

    @staticmethod
    def create_discovery(loop,
                         host_ip="0.0.0.0",
                         broadcast_ip="192.168.1.255",
                         discovery_interval=60,
                         device_discovered=None):
        return loop.create_datagram_endpoint(
            partial(FluxDiscoveryServerProtocol, loop,
                    broadcast_ip, discovery_interval, device_discovered),
            local_addr=(host_ip, FluxDiscoveryServerProtocol.DISCOVERY_PORT),
            reuse_address=True)