import asyncio
import logging

from aioflux_simple import FluxLedRgbwWifiProtocol, \
    FluxDiscoveryDeviceInfo

logging.basicConfig(level=logging.DEBUG)

loop = asyncio.get_event_loop()


def device_discovered(device: FluxDiscoveryDeviceInfo):
    print(device)


def state_change_callback(device: FluxLedRgbwWifiProtocol):
    print(device)


def disconnect_callback(exc):
    print(exc)


device_client = FluxLedRgbwWifiProtocol.create_connection(
    loop, FluxDiscoveryDeviceInfo("192.168.1.202", "", ""),
    state_change_callback=state_change_callback,
    disconnect_callback=disconnect_callback)

loop.run_until_complete(device_client)

# discovery = FluxDiscoveryServerProtocol.create_discovery(
#     loop, device_discovered=device_discovered)
# loop.run_until_complete(discovery)
loop.run_forever()
loop.close()