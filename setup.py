"""AIO flux library."""

from setuptools import setup

setup(name="aioflux_simple",
      version="0.1.3",
      description="",
      url="https://gitlab.com/mindig.marton/aioflux_simple",
      download_url="https://gitlab.com"
                   "/mindig.marton/aioflux_simple"
                   "/repository/archive.zip?ref=0.1.3",
      author="mindigmarton",
      license="MIT",
      packages=["aioflux_simple"],
      install_requires=[],
      zip_safe=True)